# 01-beaver-app

## TODO

- build
  - try `svelte-preprocess` instead od `svelte-ts-preprocess`
  - try webpack with ts-loader (for transpileOnly)
- styling
  - try adding scss support (https://medium.com/@sean_27490/svelte-sapper-with-sass-271fff662da9)
  - try adding tailwinds
  - maybe styling with css variables??
