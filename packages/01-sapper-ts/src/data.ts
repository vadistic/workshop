const about = /* md */ `
**Beavers** are _famously_ busy, and they turn their talents to reengineering the landscape as few other animals can. When sites are available, beavers burrow in the banks of rivers and lakes. But they also transform less suitable habitats by building dams. Felling and gnawing trees with their strong teeth and powerful jaws, they create massive log, branch, and mud structures to block streams and turn fields and forests into the large ponds that beavers love.

### Beaver Lodges

Domelike beaver homes, called lodges, are also constructed of branches and mud. They are often strategically located in the middle of ponds and can only be reached by underwater entrances. These dwellings are home to extended families of monogamous parents, young kits, and the yearlings born the previous spring.

Beavers are among the largest of rodents. They are herbivores and prefer to eat leaves, bark, twigs, roots, and aquatic plants.

### Aquatic Adaptations

These large rodents move with an ungainly waddle on land but are graceful in the water, where they use their large, webbed rear feet like swimming fins, and their paddle-shaped tails like rudders. These attributes allow beavers to swim at speeds of up to five miles an hour. They can remain underwater for 15 minutes without surfacing, and have a set of transparent eyelids that function much like goggles. Their fur is naturally oily and waterproof.

There are two species of beavers, which are found in the forests of North America, Europe, and Asia. These animals are active all winter, swimming and foraging in their ponds even when a layer of ice covers the surface.

`

export const data = {
  beaver: {
    title: 'Beaver',
    about,
    facts: [
      ['Common name', 'Beaver'],
      ['Scientific name', 'Castor canadensis'],
      ['Type', 'mammals'],
      ['Diet', 'Herbivore'],
      ['Group name', 'Colony'],
      ['Average life span in the wild', 'Up to 24 years'],
      ['Size', 'Head and body: 23 to 39 inches; tail: 7.75 to 12 inches'],
      ['Weigth', '60 pounds'],
    ],
    source: 'https://www.nationalgeographic.com/animals/mammals/b/beaver/',
  },
}
