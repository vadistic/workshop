import * as sapper from '@sapper/app'

sapper.start({
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  target: document.querySelector('#sapper')!,
})
